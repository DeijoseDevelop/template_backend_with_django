# template_backend_with_django



## Getting started

### Steps

1.RUN chmod 777 -R data

2.RUN sudo docker-compose build

3.RUN sudo docker-compose up -d

## Create superuser

1.RUN sudo docker ps

2.COPY id_docker

3.RUN sudo docker exec -it id_docker python manage.py createsuperuser


Follow the django steps to create the super user

## Create app

1.RUN sudo docker ps

2.COPY id_docker

3.RUN sudo docker exec -it id_docker python manage.py startapp api
